FROM node:latest

WORKDIR /usr/src/app

COPY ["package.json", "package-lock.json", "./"]
RUN npm install

COPY . .
EXPOSE 8888

## Add the wait script to the image
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait

## Launch the wait tool and then your application
CMD /wait && npm start
