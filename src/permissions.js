/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { rule, shield, not } from 'graphql-shield'

const isAuthenticated = rule()(async (root, args, ctx) => {
  return ctx.me !== undefined
})

const permissions = shield({
  Query: {
    rooms: isAuthenticated,
    notifications: isAuthenticated,
    wishes: isAuthenticated,
    wish: isAuthenticated
  },
  Mutation: {
    loginMember: not(isAuthenticated),
    registerMember: not(isAuthenticated),
    // quickRegisterMember: not(isAuthenticated),
    updateMember: isAuthenticated,
    createWish: isAuthenticated,
    updateWish: isAuthenticated,
    createRoom: isAuthenticated,
    updateRoom: isAuthenticated,
    markNotificationRead: isAuthenticated,
    makeToss: isAuthenticated
  },
  Subscription: {
    notificationPushed: isAuthenticated
  }
})

export default permissions
