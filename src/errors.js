/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { createError } from 'apollo-errors'

export const LoginError = createError('LoginError', {
  message: 'Ошибка авторизации'
})

export const PermissionDenied = createError('PermissionDenied', {
  message: 'У вас нет прав'
})
