/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import moment from 'moment'

const Room = mongoose.model('Room')
const Member = mongoose.model('Member')
const Wish = mongoose.model('Wish')
const Notification = mongoose.model('Notification')
const Conversation = mongoose.model('Conversation')

/**
 * Code: http://www.programming-algorithms.net/article/43676/Fisher-Yates-shuffle
 * @param {array} array
 */
function shuffle (array) {
  let counter = array.length

  while (counter > 0) {
    const index = Math.floor(Math.random() * counter)

    counter--

    const temp = array[counter]
    array[counter] = array[index]
    array[index] = temp
  }

  return array
}

/**
 * Rooms resolvers
 */
const RoomsResolvers = {
  Query: {
    room: (root, args) => Room.findOne({ slug: args.slug }),
    rooms: async (root, args, ctx) => {
      await Member.populate(ctx.me, { path: 'rooms' })
      return ctx.me.rooms
    }
  },

  Room: {
    isMyRoom: (root, args, ctx) => root.owner.equals(ctx.me._id),
    createdAt: (root, args) => moment(root.createdAt).format(args.format),
    members: async (root) => {
      await Room.populate(root, { path: 'members' })
      return root.members
    },
    owner: async (root) => {
      await Room.populate(root, { path: 'owner' })
      return root.owner
    },
    myWish: (root, args, ctx) => Wish.findOne({ author: ctx.me._id, room: root._id }),
    meHave: (root, args, ctx) => {
      if (!ctx.me) {
        return null
      }
      return root.members.some(member => member.equals(ctx.me._id))
    }
  },

  Mutation: {
    createRoom: async (root, args, ctx) => {
      const room = await Room.create({
        ...args,
        owner: ctx.me._id
      })

      const member = await Member.findByIdAndUpdate(ctx.me._id, {
        $addToSet: {
          rooms: room._id
        }
      })

      const conversation = await Conversation.create({
        admin: member._id,
        type: 'public',
        room: room._id
      })

      room.conversation = conversation._id
      await room.save()

      const notification = await Notification.create({
        receiver: member._id,
        type: 'ROOM_SUCCESSFUL_CREATED',
        room: room._id
      })

      ctx.pubsub.publish('NOTIFICATION_PUSHED', {
        notificationPushed: notification
      })

      return room
    },

    updateRoom: async (root, args) => {
      const room = await Room.findOne({ slug: args.slug })

      if (!room) {
        throw new Error('Комната не найдена')
      }
    },

    makeToss: async (root, args, ctx) => {
      const room = await Room.findById(args.room)

      if (!room.owner.equals(ctx.me._id)) {
        throw new Error('Вы не владелец комнаты')
      }

      if (room.members.length < 3) {
        throw new Error('Не хватает участников')
      }

      /**
       * Shuffle members to help find Santa
       * Using Fisher–Yates Shuffle algoritm
       */
      const shuffled = shuffle([...room.members])

      /**
       * Find secret santa for each members of the room
       */
      await Promise.all(shuffled.map((author, i) =>
        Wish.findOneAndUpdate({ author, room: room._id }, {
          santa: shuffled[i + 1] || shuffled[0]
        })
      ))

      /**
       * Mark for current room that the game has started
       */
      room.isPlayed = true
      await room.save()

      /**
       * Send notifications for all members in the current room
       */
      await Promise.all(room.members.map(async receiver => {
        const notification = await Notification.create({
          receiver,
          type: 'ROOM_WAS_TOSSED',
          room: room._id
        })
        ctx.pubsub.publish('NOTIFICATION_PUSHED', {
          notificationPushed: notification
        })
      }))

      return true
    }
  }
}

export default RoomsResolvers
