/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import moment from 'moment'
import jwt from 'jsonwebtoken'
import config from '../../config'

const Member = mongoose.model('Member')
const Wish = mongoose.model('Wish')
const Notification = mongoose.model('Notification')

/**
 * Member resolvers
 */
const MembersResolvers = {
  Query: {
    member: (root, args) => Member.findById(args._id),
    me: (root, args, ctx) => {
      if (!ctx.me) {
        return null
      }
      return ctx.me
    },
    count: () => Member.estimatedDocumentCount()
  },

  Member: {
    createdAt: (root, args) => moment(root.createdAt).format(args.format)
  },

  Me: {
    rooms: async (root, args) => {
      await Member.populate(root, {
        path: 'rooms',
        options: {
          limit: 3,
          sort: {
            createdAt: -1
          }
        }
      })
      return root.rooms
    },
    wishes: (root, args) => Wish.find({ author: root._id, ...args })
  },

  Mutation: {
    registerMember: async (root, args, ctx) => {
      const existMember = await Member.findOne({ email: args.email })

      if (existMember) {
        throw new Error('Такой email уже используется')
      }

      const hash = await Member.generateHash(args.password)
      const member = await Member.create({ ...args, password: hash })

      const notification = await Notification.create({
        receiver: member._id,
        type: 'MEMBER_SUCCESS_REGISTER'
      })
      ctx.pubsub.publish('NOTIFICATION_PUSHED', {
        notificationPushed: notification
      })

      return jwt.sign({ _id: member._id }, config.get('secret').jwt)
    },

    // quickRegisterMember: async (root, args) => {
    //   const member = await Member.create(args)
    //   return jwt.sign({ _id: member._id }, config.get('secret').jwt)
    // },

    updateMember: async (root, args, ctx) => {
      let fields = { ...args }

      if (fields.email) {
        const existMember = await Member.findOne({
          email: fields.email,
          _id: { $ne: ctx.me._id }
        })
        if (existMember) {
          throw new Error('Такой email уже используется')
        }
      }

      if (fields.password) {
        const hash = await Member.generateHash(args.password)
        fields = { ...fields, password: hash }
      }

      return Member.findByIdAndUpdate(ctx.me._id, fields, { new: true })
    },

    loginMember: (root, args) => Member.authentication(args),

    emailIsExist: async (root, args) => {
      const member = await Member.findOne({ email: args.email })
      if (!member) {
        return false
      }
      return true
    }
  }
}

export default MembersResolvers
