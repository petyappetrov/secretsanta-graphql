/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import merge from 'merge'

import MembersSchema from './members/schema.graphql'
import MembersResolvers from './members/resolver'

import RoomsSchema from './rooms/schema.graphql'
import RoomsResolvers from './rooms/resolver'

import ConversationsSchema from './conversations/schema.graphql'
import ConversationResolvers from './conversations/resolver'

import WishesSchema from './wishes/schema.graphql'
import WishResolvers from './wishes/resolver'

import NotificationsSchema from './notifications/schema.graphql'
import NotificationResolvers from './notifications/resolver'

/**
 * Types schemas
 */
export const typeDefs = [
  MembersSchema,
  RoomsSchema,
  ConversationsSchema,
  WishesSchema,
  NotificationsSchema
]

/**
 * Resolvers
 */
export const resolvers = merge.recursive(
  MembersResolvers,
  RoomsResolvers,
  ConversationResolvers,
  WishResolvers,
  NotificationResolvers
)
