/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'

const Room = mongoose.model('Room')
const Member = mongoose.model('Member')
const Wish = mongoose.model('Wish')
const Notification = mongoose.model('Notification')
const Conversation = mongoose.model('Conversation')

/**
 * Member resolvers
 */
const WishesResolvers = {
  Query: {
    wishes: (root, args, ctx) => Wish.find({ author: ctx.me._id }),
    wish: (root, args, ctx) => Wish.findOne({ room: args.room, santa: ctx.me._id })
  },

  Wish: {
    author: async (root) => {
      await Wish.populate(root, { path: 'author' })
      return root.author
    }
  },

  Mutation: {
    createWish: async (root, args, ctx) => {
      const exist = await Wish.findOne({ author: ctx.me._id, room: args.room })

      if (exist) {
        throw new Error('У вас уже существует пожелание')
      }

      const wish = await Wish.create({
        ...args,
        author: ctx.me._id
      })

      const room = await Room.findByIdAndUpdate(args.room, {
        $addToSet: {
          members: ctx.me._id
        }
      }, {
        new: true
      })

      const conversation = await Conversation.findByIdAndUpdate(room.conversation, {
        $addToSet: {
          participants: ctx.me._id
        }
      }, {
        new: true
      })

      const member = await Member.findByIdAndUpdate(ctx.me._id, {
        $addToSet: {
          rooms: args.room,
          conversations: conversation._id
        }
      }, {
        new: true
      })

      await Promise.all(room.members.map(async receiver => {
        const notification = await Notification.create({
          receiver,
          type: receiver.equals(ctx.me._id)
            ? 'YOU_SUCCESS_ADDED_TO_ROOM'
            : 'MEMBER_SUCCESS_ADDED_TO_ROOM',
          room: room._id,
          member: member._id
        })
        ctx.pubsub.publish('NOTIFICATION_PUSHED', {
          notificationPushed: notification
        })
      }))

      return wish
    },

    updateWish: async (root, args, ctx) => {
      const wish = await Wish.findOneAndUpdate({ room: args.room, author: ctx.me._id }, args, { new: true })

      if (wish.santa) {
        const notification = await Notification.create({
          receiver: wish.santa,
          type: 'WISH_HAS_BEEN_UPDATED',
          room: args.room
        })
        ctx.pubsub.publish('NOTIFICATION_PUSHED', {
          notificationPushed: notification
        })
      }

      return wish
    }
  }
}

export default WishesResolvers
