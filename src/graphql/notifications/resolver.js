/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import moment from 'moment'
import { withFilter } from 'graphql-subscriptions'

const Notification = mongoose.model('Notification')

const NotificationsResolvers = {
  Subscription: {
    notificationPushed: {
      subscribe: withFilter(
        (root, args, ctx) => ctx.pubsub.asyncIterator(['NOTIFICATION_PUSHED']),
        (payload, variables, ctx) => {
          if (payload) {
            return payload.notificationPushed.receiver.equals(ctx.me._id)
          }
        }
      )
    }
  },
  Query: {
    notifications: (root, args, ctx) =>
      Notification.getPaginatedList({ receiver: ctx.me._id, ...args })
  },
  Notification: {
    createdAt: (root, args) => moment(root.createdAt).format(args.format),
    __resolveType: (root, args, ctx) => {
      switch (root.type) {
        case 'YOU_SUCCESS_ADDED_TO_ROOM':
          return 'NotificationYouSuccessAddedToRoom'
        case 'MEMBER_SUCCESS_ADDED_TO_ROOM':
          return 'NotificationMemberSuccessAddedToRoom'
        case 'ROOM_WAS_TOSSED':
          return 'NotificationRoomWasTossed'
        case 'WISH_HAS_BEEN_UPDATED':
          return 'NotificationWishHasBeenUpdated'
        case 'ROOM_SUCCESSFUL_CREATED':
          return 'NotificationRoomSuccessfulCreated'
        case 'MEMBER_SUCCESS_REGISTER':
          return 'NotificationMemberSuccessRegister'
        default:
          return null
      }
    }
  },
  NotificationYouSuccessAddedToRoom: {
    room: async (root, args, ctx) => {
      await Notification.populate(root, { path: 'room' })
      return root.room
    }
  },
  NotificationMemberSuccessAddedToRoom: {
    room: async (root, args, ctx) => {
      await Notification.populate(root, { path: 'room' })
      return root.room
    },
    member: async (root, args, crx) => {
      await Notification.populate(root, { path: 'member' })
      return root.member
    }
  },
  NotificationRoomWasTossed: {
    room: async (root, args, ctx) => {
      await Notification.populate(root, { path: 'room' })
      return root.room
    }
  },
  NotificationWishHasBeenUpdated: {
    room: async (root, args, ctx) => {
      await Notification.populate(root, { path: 'room' })
      return root.room
    }
  },
  NotificationRoomSuccessfulCreated: {
    room: async (root, args, ctx) => {
      await Notification.populate(root, { path: 'room' })
      return root.room
    }
  },
  Mutation: {
    markNotificationRead: (root, args) =>
      Notification.findByIdAndUpdate(args.notification, { isRead: true }, { new: true })
  }
}

export default NotificationsResolvers
