/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import moment from 'moment'
import { withFilter } from 'graphql-subscriptions'

const Conversation = mongoose.model('Conversation')
const Message = mongoose.model('Message')
const Member = mongoose.model('Member')
const Wish = mongoose.model('Wish')

const ConversationsResolvers = {
  Subscription: {
    messageAdded: {
      subscribe: withFilter(
        (root, args, ctx) => ctx.pubsub.asyncIterator(['MESSAGE_ADDED']),
        (payload, variables) => {
          return payload.messageAdded.conversation.equals(variables.conversation)
        }
      )
    }
  },
  Query: {
    conversation: (root, args) => Conversation.findById(args.conversation),
    conversations: async (root, args, ctx) => {
      return Promise.all(ctx.me.conversations.map(id => Conversation.findById(id)))
    }
  },
  Conversation: {
    messages: (root, args) => Message.getPaginatedList({ ...args, conversation: root._id }),
    participants: async (root, args) => {
      await Conversation.populate(root, { path: 'participants' })
      return root.participants
    },
    room: async (root, args) => {
      await Conversation.populate(root, { path: 'room' })
      return root.room
    }
  },
  Message: {
    author: async (root, args, ctx) => {
      const conversation = await Conversation.findById(root.conversation)
      if (
        conversation.type === 'public' ||
        conversation.admin === ctx.me._id ||
        root.author.equals(ctx.me._id)
      ) {
        await Message.populate(root, { path: 'author' })
        return root.author
      }
      return null
    },
    createdAt: (root, args) => moment(root.createdAt).format(args.format)
  },
  Mutation: {
    addMessage: async (root, args, ctx) => {
      const message = await Message.create({
        ...args,
        author: ctx.me._id
      })

      ctx.pubsub.publish('MESSAGE_ADDED', { messageAdded: message })
      return message
    },

    createWishConversation: async (root, args, ctx) => {
      const wish = await Wish.findById(args.wish)

      if (!wish.santa.equals(ctx.me._id)) {
        throw new Error('Ты чо, хацкер что ли?')
      }

      const conversation = await Conversation.create({
        type: 'anonymous',
        admin: wish.santa,
        participants: [wish.santa, wish.author],
        room: wish.room
      }, { new: true })

      wish.conversation = conversation._id
      await wish.save()

      await Promise.all(conversation.participants.map(participant =>
        Member.findByIdAndUpdate(participant, {
          $addToSet: {
            conversations: conversation._id
          }
        })
      ))

      return conversation._id
    }
  }
}

export default ConversationsResolvers
