/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import convict from 'convict'
import path from 'path'

/**
 * Schema for env variables
 */
const config = convict({
  env: {
    env: 'NODE_ENV',
    doc: 'The application environment.',
    format: [
      'production',
      'development'
    ],
    default: 'development'
  },
  port: {
    env: 'PORT',
    doc: 'The port to bind.',
    format: 'port',
    default: 8888
  },
  db: {
    host: {
      doc: 'Database host name/IP',
      format: '*',
      default: 'mongodb://127.0.0.1:27017'
    },
    name: {
      doc: 'Database name',
      format: String,
      default: 'secretsanta'
    }
  },
  secret: {
    jwt: {
      doc: 'Secret text for JSON Web Token',
      format: String,
      default: 'b8fb39b4468a43f3b8aa732d06a687ec'
    }
  }
})

config.loadFile(path.join(__dirname, config.get('env') + '.json'))

export default config
