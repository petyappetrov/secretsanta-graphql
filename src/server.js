/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { GraphQLServer, PubSub } from 'graphql-yoga'
import { makeExecutableSchema } from 'graphql-tools'
import jwt from 'jsonwebtoken'
import { formatError } from 'apollo-errors'
import path from 'path'
import express from 'express'
import mongoose from 'mongoose'
import { typeDefs, resolvers } from './graphql'
import permissions from './permissions'
import config from './config'

const pubsub = new PubSub()
const Member = mongoose.model('Member')

const removeBearer = (token) => {
  const parts = token.split(' ')
  if (parts.length === 2) {
    if (/^Bearer$/i.test(parts[0])) {
      return parts[1]
    }
  }
}

const getToken = (request, connection) => {
  const { authToken } = (connection || {}).context || {}
  if (authToken) {
    return removeBearer(authToken)
  }

  const { authorization } = (request || {}).headers || {}
  if (!authorization) {
    return null
  }

  return removeBearer(authorization)
}

/**
 * Passing decoded JWT to context
 */
const context = async ({ request, response, connection }) => {
  const token = getToken(request, connection)

  if (!token) {
    return { pubsub }
  }

  const data = await jwt.verify(token, config.get('secret').jwt)
  const me = await Member.findById(data._id)

  if (!me) {
    return { pubsub }
  }

  return {
    me,
    pubsub
  }
}

/**
 * Initialize server
*/
const server = new GraphQLServer({
  schema: makeExecutableSchema({
    typeDefs,
    resolvers,
    resolverValidationOptions: {
      requireResolversForResolveType: false
    },
    inheritResolversFromInterfaces: true
  }),
  context,
  middlewares: [permissions]
})

/**
 * Express configuration
 */
server.express.use('/static', express.static(path.join(__dirname, '..', 'static')))

/**
 * Start application
 */
const options = {
  formatError,
  port: config.get('port'),
  endpoint: '/graphql',
  subscriptions: '/subscriptions',
  playground: '/'
}
server.start(options, ({ port }) => {
  console.log(`✅  Server ready at ${port}`)
})
