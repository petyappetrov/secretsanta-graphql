/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'

/**
 * Wish schema
 */
const WishSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true
  },
  room: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Room',
    required: true
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Member',
    required: true
  },
  santa: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Member'
  },
  conversation: {
    type: mongoose.Schema.Types.ObjectId
  }
}, {
  versionKey: false,
  timestamps: true
})

mongoose.model('Wish', WishSchema)
