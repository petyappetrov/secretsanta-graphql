/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'

/**
 * Conversation schema
 */
const ConversationSchema = new mongoose.Schema({
  participants: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Member'
  }],
  admin: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  room: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Room',
    required: true
  },
  type: {
    type: String,
    required: true,
    enum: [
      'public',
      'anonymous'
    ]
  }
}, {
  versionKey: false
})

mongoose.model('Conversation', ConversationSchema)
