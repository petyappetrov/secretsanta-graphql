/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'

/**
 * Notification schema
 */
const NotificationSchema = new mongoose.Schema({
  receiver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Member',
    required: true
  },
  isRead: {
    type: Boolean,
    default: false
  },
  type: {
    type: String,
    required: true,
    enum: [
      'YOU_SUCCESS_ADDED_TO_ROOM',
      'MEMBER_SUCCESS_ADDED_TO_ROOM',
      'MEMBER_SUCCESS_REGISTER',
      'ROOM_WAS_TOSSED',
      'ROOM_SUCCESSFUL_CREATED',
      'WISH_HAS_BEEN_UPDATED'
    ]
  },
  member: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Member'
  },
  room: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Room'
  }
}, {
  versionKey: false,
  timestamps: true,
  strict: false
})

/**
 * Statics
 */
NotificationSchema.statics = {
  async getPaginatedList ({
    receiver,
    limit = 10,
    skip = 0,
    isRead
  }) {
    const match = {
      receiver: mongoose.Types.ObjectId(receiver)
    }

    if (typeof isRead === 'boolean') {
      match.isRead = isRead
    }

    const results = await this.aggregate([
      {
        $match: match
      },
      {
        $sort: {
          createdAt: -1
        }
      },
      {
        $group: {
          _id: null,
          count: {
            $sum: 1
          },
          items: {
            $push: '$$ROOT'
          }
        }
      },
      {
        $project: {
          count: 1,
          items: {
            $slice: ['$items', skip, limit]
          }
        }
      }
    ])
    return results[0] || { items: [], count: 0 }
  }
}

mongoose.model('Notification', NotificationSchema)
