/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'

/**
 * Message schema
 */
const MessageSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true
  },
  conversation: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Member'
  }
}, {
  versionKey: false,
  timestamps: true
})

/**
 * Statics
 */
MessageSchema.statics = {
  async getPaginatedList ({
    conversation,
    limit = 10,
    skip = 0
  }) {
    const results = await this.aggregate([
      {
        $match: {
          conversation: mongoose.Types.ObjectId(conversation)
        }
      },
      {
        $sort: {
          createdAt: -1
        }
      },
      {
        $group: {
          _id: null,
          count: {
            $sum: 1
          },
          items: {
            $push: '$$ROOT'
          }
        }
      },
      {
        $project: {
          count: 1,
          items: {
            $reverseArray: {
              $slice: ['$items', skip, limit]
            }
          }
        }
      }
    ])
    return results[0] || { items: [], count: 0 }
  }
}

mongoose.model('Message', MessageSchema)
