/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import Joi from 'joi'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
// import jdenticon from 'jdenticon'
// import fs from 'fs'
import { LoginError } from '../../errors'
import config from '../../config'

// jdenticon.config = {
//   lightness: {
//     color: [0.47, 0.47],
//     grayscale: [0.37, 0.52]
//   },
//   saturation: {
//     color: 1.00,
//     grayscale: 0.92
//   },
//   backColor: '#fff'
// }

/**
 * Member schema
 */
const MemberSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  avatar: {
    type: String,
    url: String
  },
  email: {
    type: String,
    trim: true,
    unique: true,
    required: true,
    validate: [
      (email) => Joi.validate(email, Joi.string().email()),
      'Неверная электронная почта'
    ]
  },
  rooms: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Room'
  }],
  conversations: [{
    type: mongoose.Schema.Types.ObjectId
  }],
  locale: {
    type: String,
    default: 'ru',
    enum: ['ru', 'en']
  },
  theme: {
    type: String,
    default: 'light',
    enum: ['dark', 'light']
  }
}, {
  versionKey: false,
  timestamps: true
})

/**
 * Statics
 */
MemberSchema.statics = {
  async authentication ({ email, password }) {
    const member = await this.findOne({ email })
    if (!member) {
      throw new LoginError({
        data: {
          email: 'Пользователь с таким email адресом не зарегистрирован'
        }
      })
    }

    const isMatch = await bcrypt.compare(password, member.password)

    if (!isMatch) {
      throw new LoginError({
        data: {
          password: 'Неверный пароль'
        }
      })
    }

    return jwt.sign({ _id: member._id }, config.get('secret').jwt)
  },

  async generateHash (password) {
    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(password, salt)
    return hash
  }
}

/**
 * Hooks
 */
// MemberSchema.pre('save', function (next) {
//   if (!this.avatar) {
//     const png = jdenticon.toPng(this.email, 200)
//     const photoPath = 'static/' + this._id + '-avatar.png'
//     fs.writeFileSync(photoPath, png)
//     this.avatar = photoPath
//   }
//   next()
// })

mongoose.model('Member', MemberSchema)
