/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */
import mongoose from 'mongoose'
import URLSlugs from 'mongoose-url-slugs'
import translit from 'translitit-cyrillic-russian-to-latin'

/**
 * Room schema
 */
const RoomSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  slug: {
    type: String,
    unique: true,
    required: true
  },
  description: String,
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Member',
    required: true
  },
  conversation: {
    type: mongoose.Schema.Types.ObjectId
  },
  members: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Member'
  }],
  isPlayed: {
    type: Boolean,
    default: false
  },
  sum: {
    type: Number,
    required: true
  }
}, {
  versionKey: false,
  timestamps: true
})

/**
 * Plugins
 */
RoomSchema.plugin(URLSlugs('name', {
  indexUnique: true,
  generator: (text) => translit(text).trim().toLowerCase().replace(/([^a-z0-9\-\\_]+)/g, '-')
}))

mongoose.model('Room', RoomSchema)
